#!/usr/bin/env python3

import configparser
from influxdb import InfluxDBClient
import json
import sys
import os
from influxdb import InfluxDBClient
import json
from datetime import datetime
from time import time, sleep
import pytz
import argparse
import socket
import fcntl
import sys
import atexit


# Obtain an exclusive lock on a file
lock_file = open('/tmp/my_script.lock', 'w')
try:
    fcntl.lockf(lock_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
except IOError:
    sys.exit(0)  # Exit if the lock cannot be obtained

# Function to release the lock when the script exits
def release_lock():
    lock_file.close()

# Register the release_lock function to be called on script exit
atexit.register(release_lock)

parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--host', type=str, required=False)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    tz_string = config['DETAILS']['TIMEZONE']



# Some variables
tz = pytz.timezone(tz_string)
#print("Timezone: " + str(tz))

# Create and select influx database
client = InfluxDBClient(host=influx_host, port=8086)
client.create_database('uptime')
client.get_list_database()
client.switch_database('uptime')

if args.host:
    fqdn_hostname = args.host
else:
    fqdn_hostname = socket.gethostname() + "-remote"

while True:
    sleep(60 - time() % 60)

    tz_now = datetime.now(tz)
    offset = tz_now.utcoffset()
    time_now = datetime.time(tz_now)
    date_now = datetime.date(tz_now)
    datetime_now = str(date_now) + " " + str(time_now.hour) + ":" + str(time_now.minute) + ":00.000000+" + str(offset)
    formattedtime = tz_now.strftime("%Y-%m-%d %H:%M:00.000000%z")
    tz_now_rfc3339 = tz_now.isoformat('T') + "Z"

#    print("TZ Now: " + str(tz_now))
#    print("FQDN: " + fqdn_hostname)
#    print("Date time now: " + datetime_now)
#    print("Offset: " + str(offset))
#    print("Full time now rfc3339: " + tz_now_rfc3339)
#    print("Full formatted time now: " + formattedtime)

    json_body = [{
        "measurement": "tracking",
        "tags": {
            "host": fqdn_hostname
        },
        "time": formattedtime,
        "fields": {
            "up": 1
        }
    }]

    print("Writing to Influxdb")

    client.write_points(json_body)

exit()
